import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		new Thread(new ToRun()).start();
		/*yes, new keyword can be used with interfaces while creating anonymous classes that implements an interface*/
		new Thread(new Runnable() {
			public void run() {
				System.out.println("hello from anonlymous class's run method");
			}
		}).start();
		/*
		 * Every lambda expression is composed of 3 parts:
		 * Argument list: ()
		 * Arrow token: ->
		 * Body: System.out.println("hello from lambda function")
		 * 
		 * When the compiler sees a Lambda expression how does it know what to do?
		 * 
		 * It knows that one of the Thread class's constructors accepts a Runnable. 
		 * It also knows that the Runnable interface has only one method -"run" method- which takes no parameters. 
		 * So it matches the lambda expression's argument list with the "run" method.
		 * 
		 * Because the compiler needs to match a lambda expression to a method, lambda expressions can only be used with interfaces
		 * that contain only 1 method that has to be implemented. These interfaces are also referred to as Functional Interfaces.
		 * 
		 * As in the example I am showing here, Instead of creating a class that implements Runnable or writing an Anonymous classes, 
		 * we can use lambda expressions. So, we are able to reduce the line of codes we have to write and we can focus on what we care about 
		 * (the code that we want to run).
		 * 
		 * In intelliJ the editor detects the lambda expressions and creates a link with a lambda symbol. Once clicked, it takes you to the method 
		 * that intelliJ believes that the lambda expression is mapped to. In this example, of course, 
		 * it will take you to the Runnable interface's run method.
		 * */
		new Thread(()->System.out.println("hello from lambda expression")).start();
		
		/*You could of course execute multiple expressions in a lambda expression*/
		new Thread(()->{
			System.out.println("hello from 2nd lambda expression");
			System.out.println("hello from 2nd lambda expression's 2nd statement");
		}).start();
		
		Employee serdar = new Employee(38, "serdar");
		Employee osman = new Employee(27, "osman");
		Employee onur = new Employee(78, "onur");
		
		List<Employee> employees = new ArrayList<>();
		employees.add(serdar);
		employees.add(osman);
		employees.add(onur);
		
		
		// sorting without Lambda expressions
		 
//		Collections.sort(employees, new Comparator<Employee>() {
//
//			@Override
//			public int compare(Employee e1, Employee e2) {
//				return e1.getName().compareTo(e2.getName());
//			}
//		});
		
		/*
		 * When you check the documentation for Comparator interface you see there are 2 methods without implementation. "compare" and "equals".
		 * Lambda expressions can be used with interfaces that require the implementation of only one method (Functional Interfaces).
		 * Comparator documentation states that this interface is a Functional Interface and can be used as the assignment target for a Lambda expression. 
		 * How does that happen? There seems like there are not 1 but 2 methods to be implemented here?
		 * 
		 * equals method will always have a default implementation. Remember that all classes descent from "Object" and "Object" contains an "equals" method.
		 * So, every instance that implements Comparator will already have an implementation for the "equals" method. Therefore Comparator interface has 
		 * one method that has to be implemented by the classes that implement Comparator interface. For that reason it is a functional interface and 
		 * we can use Lambda expressions.
		 *
		 * */
		
		
		// sorting with lambda expressions
		 
		//Collections.sort(employees, (Employee e1, Employee e2) -> e1.getName().compareTo(e2.getName()));
		
		/*
		 * we can further simplify the Lambda expression by removing the type declarations as below.
		 * compiler can infer from the "employees" parameter that the objects to be compared are of type "Employee". 
		 * 
		 * Another important point:
		 * When Lambda expression's body is a single statement that evaluates to a value the "return" keyword is assumed. 
		 * Return value is inferred to be the type of the evaluated value which in our case here is String.
		 * If our lambda expression had multiple statements then we would have to use the return statement.
		 * */
		Collections.sort(employees, (e1, e2) -> e1.getName().compareTo(e2.getName()));
		
		for(Employee e : employees) {
			System.out.println(e.getName());
		}
		
		/*
		 * Another use of Lambda expressions:
		 * Here we are creating an implementation as a Lambda expression for our Functional Interface MyConcat defined below.
		 * And we are providing this Lambda expression as a parameter to doStrings method.
		 * */
		MyConcat mc = (s1, s2) -> s1 + " - " + s2;
		String aString = doStrings(mc, employees.get(2).getName(), employees.get(0).getName());
		System.out.println(aString);
	}
	
	public static final String doStrings(MyConcat mc, String s1, String s2) {
		return mc.concatStrings(s1, s2);
	}

}

class ToRun implements Runnable{

	@Override
	public void run() {
		System.out.println("hello from ToRun Class's run method.");
	}
	
}

class Employee{
	
	private int age;
	private String name;
	
	public Employee(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

interface MyConcat{
	public String concatStrings(String s1, String s2);
}
